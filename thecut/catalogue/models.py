# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.core.urlresolvers import reverse
from django.db import models
from thecut.catalogue import querysets
from thecut.ordering.models import OrderMixin
from thecut.publishing.models import Content, SiteContentWithSlug

from .receivers import update_categories


try:
    from django.utils.encoding import python_2_unicode_compatible
except ImportError:
    from thecut.publishing.utils import python_2_unicode_compatible


@python_2_unicode_compatible
class AbstractAttribute(models.Model):

    name = models.CharField(max_length=200)

    class Meta(object):
        abstract = True
        ordering = ['name']

    def __str__(self):
        return '{0}'.format(self.name)


class Attribute(AbstractAttribute):

    objects = querysets.AttributeQuerySet.as_manager()

    def get_absolute_url(self):
        return reverse('catalogue:attribute_detail', kwargs={'pk': self.pk})


class AbstractBrand(SiteContentWithSlug):

    class Meta(SiteContentWithSlug.Meta):
        abstract = True


class Brand(AbstractBrand):

    def get_absolute_url(self):
        # TODO: product_brand_list, with category slug as kwarg?
        return reverse('catalogue:brand_detail', kwargs={'slug': self.slug})


class AbstractRange(SiteContentWithSlug):

    class Meta(SiteContentWithSlug.Meta):
        abstract = True


class Range(AbstractRange):

    brand = models.ForeignKey('catalogue.Brand')


class AbstractCategory(SiteContentWithSlug):

    class Meta(SiteContentWithSlug.Meta):
        abstract = True


class Category(AbstractCategory):

    def get_absolute_url(self):
        # TODO: product_category_list, with category slug as kwarg?
        return reverse('catalogue:category_detail', kwargs={'slug': self.slug})


class AbstractProduct(SiteContentWithSlug):

    class Meta(SiteContentWithSlug.Meta):
        abstract = True


class AbstractProductWithPrice(AbstractProduct):

    """Reference product model with fixed price and SKU."""

    sku = models.CharField(max_length=200, db_index=True, unique=True,
                           blank=True, default='')
    """Unique :py:class:`unicode` stock-keeping unit."""

    price = models.DecimalField(max_digits=7, decimal_places=2, blank=True,
                                null=True)
    """:py:class:`decimal.Decimal` listed price."""

    class Meta(AbstractProduct.Meta):
        abstract = True


class Product(AbstractProduct):

    brand = models.ForeignKey('catalogue.Brand', related_name='products',
                              null=True, blank=True, on_delete=models.SET_NULL)

    range = models.ForeignKey('catalogue.Range', related_name='products',
                              null=True, blank=True, on_delete=models.PROTECT)

    primary_category = models.ForeignKey('catalogue.Category',
                                         related_name='primary_products+',
                                         null=True, blank=True,
                                         on_delete=models.SET_NULL)
    """Primary :py:class:`~thecut.catalogue.models.Category` (optional)."""

    categories = models.ManyToManyField('catalogue.Category',
                                        blank=True, related_name='products',
                                        through='catalogue.ProductCategory')
    """Multiple :py:class:`~thecut.catalogue.models.Category` (optional)."""

    def get_absolute_url(self):
        return reverse('catalogue:product_detail', kwargs={'slug': self.slug})

    _cached_attributes = None

    def get_common_attributes(self):
        """
        Return list of used attributes in skus.

        :return: list of :py:class:'project.products.models.Attribute'
        """
        if not self._cached_attributes:
            self._cached_attributes = Attribute.objects.used_in_product(self)
        return self._cached_attributes


models.signals.post_save.connect(update_categories, sender=Product)


class ProductCategory(OrderMixin):

    product = models.ForeignKey('catalogue.Product',
                                related_name='categoryproducts+')

    category = models.ForeignKey('catalogue.Category',
                                 related_name='categoryproducts+')

    class Meta(OrderMixin.Meta, SiteContentWithSlug.Meta):
        unique_together = [('product', 'category')]


class AbstractSKU(Content):

    """
    A stock-keeping unit (product variant).

    An SKU is a distinct item, such as a product or service, as it is offered
    for sale that embodies all attributes associated with the item and that
    distinguish it from all other items.
    """

    sku = models.CharField(max_length=200, db_index=True)
    """Unique :py:class:`unicode` stock-keeping unit (required)."""

    class Meta(Content.Meta):
        abstract = True


class SKU(AbstractSKU):

    """
    A stock-keeping unit (product variant).

    An SKU is a distinct item, such as a product or service, as it is offered
    for sale that embodies all attributes associated with the item and that
    distinguish it from all other items.
    """

    product = models.ForeignKey('catalogue.Product', related_name='skus',
                                on_delete=models.PROTECT)
    """Related :py:class:`~thecut.catalogue.models.Product` (required)."""

    def get_absolute_url(self):
        return reverse('catalogue:sku_detail', kwargs={'pk': self.pk})

    def get_common_sku_attributes(self):
        """
        Return skuattributes that assigned to any SKUs of the parent product.

        If some of the attributes are not assigned to this SKU,
        None is return in its place.

        :return: list of :py:class:'thecut.catalogue.SKUAttribute'
        """
        skuattributes = {}
        for skuattribute in self.skuattributes.all():
            skuattributes[skuattribute.attribute_id] = skuattribute

        common = []
        for attribute in self.product.get_common_attributes():
            if attribute.id in skuattributes:
                common.append(skuattributes[attribute.id])
            else:
                common.append(None)
        return common


class AbstractSKUAttribute(models.Model):

    value = models.CharField(max_length=200)
    """:py:class:`unicode` attribute value (required)."""

    class Meta(object):
        abstract = True


@python_2_unicode_compatible
class SKUAttribute(AbstractSKUAttribute):

    sku = models.ForeignKey('catalogue.SKU', related_name='skuattributes')
    """Related :py:class:`~thecut.catalogue.models.SKU` (required)."""

    attribute = models.ForeignKey('catalogue.Attribute',
                                  related_name='skuattributes',
                                  on_delete=models.PROTECT)
    """Related :py:class:`~thecut.catalogue.models.Attribute` (required)."""

    class Meta(object):
        unique_together = [('sku', 'attribute')]
        ordering = ['attribute__name']

    def __str__(self):
        return '{0}'.format(self.attribute)
