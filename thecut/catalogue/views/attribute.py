# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.views import generic
from thecut.catalogue import settings
from thecut.catalogue.models import Attribute


class DetailView(generic.DetailView):
    model = Attribute


class ListView(generic.ListView):
    paginate_by = settings.PRODUCTS_PAGINATE_BY
    model = Attribute

    def get_queryset(self, *args, **kwargs):
        queryset = super(ListView, self).get_queryset(*args, **kwargs)
        return queryset.not_removed()
