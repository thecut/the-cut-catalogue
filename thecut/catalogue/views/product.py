# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.shortcuts import get_object_or_404
from django.views import generic
from thecut.catalogue import settings
from thecut.catalogue.models import Brand, Category, Product


class DetailView(generic.DetailView):
    model = Product


class ListView(generic.ListView):
    paginate_by = settings.PRODUCTS_PAGINATE_BY
    model = Product

    def get_queryset(self, *args, **kwargs):
        queryset = super(ListView, self).get_queryset(*args, **kwargs)
        return queryset.not_removed()


class CategoryListView(generic.ListView):
    paginate_by = settings.PRODUCTS_PAGINATE_BY
    model = Product
    template_name = 'product_category_list.html'

    def get_category(self):
        return get_object_or_404(Category.objects.active(),
                                 slug=self.kwargs.get('slug'))

    def get_context_data(self, **kwargs):
        context_data = super(CategoryListView, self).get_context_data(**kwargs)
        category = self.get_category()
        context_data.update({'category': category})
        return context_data

    def get_queryset(self, *args, **kwargs):
        queryset = super(CategoryListView, self).get_queryset(*args, **kwargs)
        return queryset.for_category(self.get_category())


class BrandListView(generic.ListView):
    paginate_by = settings.PRODUCTS_PAGINATE_BY
    model = Product
    template_name = 'catalogue/product_brand_list.html'

    def get_brand(self):
        return get_object_or_404(Brand.objects.active(),
                                 slug=self.kwargs.get('slug'))

    def get_context_data(self, **kwargs):
        context_data = super(BrandListView, self).get_context_data(**kwargs)
        brand = self.get_brand()
        context_data.update({'brand': brand})
        return context_data

    def get_queryset(self, *args, **kwargs):
        queryset = super(BrandListView, self).get_queryset(*args, **kwargs)
        return queryset.for_brand(self.get_brand())
