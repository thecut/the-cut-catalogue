# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals


def update_categories(sender, instance, raw, **kwargs):
    """Ensure that a produt's primary category is in the category list."""
    if not raw:
        if instance.primary_category and not instance.categories.filter(
                pk=instance.primary_category.pk).exists():
            instance.categories.through.objects\
                .create(product=instance, category=instance.primary_category)
