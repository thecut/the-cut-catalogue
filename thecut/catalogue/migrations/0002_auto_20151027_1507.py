# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='sku',
            options={'ordering': ['title'], 'get_latest_by': 'publish_at'},
        ),
        migrations.RemoveField(
            model_name='attribute',
            name='removed_at',
        ),
        migrations.RemoveField(
            model_name='attribute',
            name='removed_by',
        ),
        migrations.RemoveField(
            model_name='brand',
            name='removed_at',
        ),
        migrations.RemoveField(
            model_name='brand',
            name='removed_by',
        ),
        migrations.RemoveField(
            model_name='category',
            name='removed_at',
        ),
        migrations.RemoveField(
            model_name='category',
            name='removed_by',
        ),
        migrations.RemoveField(
            model_name='product',
            name='removed_at',
        ),
        migrations.RemoveField(
            model_name='product',
            name='removed_by',
        ),
        migrations.RemoveField(
            model_name='sku',
            name='attributes_description',
        ),
        migrations.RemoveField(
            model_name='sku',
            name='removed_at',
        ),
        migrations.RemoveField(
            model_name='sku',
            name='removed_by',
        ),
    ]
