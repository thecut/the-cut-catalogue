# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.contrib import admin
from thecut.authorship.admin import AuthorshipMixin
from thecut.catalogue.models import (SKU, Attribute, Brand, Category, Product,
                                     ProductCategory, SKUAttribute)


class ProductCategoryInline(admin.TabularInline):

    model = ProductCategory
    verbose_name = 'Category'
    verbose_name_plural = 'Categories'
    extra = 1

    fields = ['category']


class ProductAdmin(AuthorshipMixin, admin.ModelAdmin):

    fieldsets = [
        (None, {'fields': ['title', 'primary_category',
                           'headline', 'featured_content', 'content',
                           'meta_description', 'tags']}),
        ('Publishing', {'fields': ['slug', ('publish_at', 'is_enabled'),
                                   'expire_at', 'publish_by', 'template',
                                   'is_featured', 'is_indexable',
                                   ('created_at', 'created_by'),
                                   ('updated_at', 'updated_by'),
                                   ('removed_at', 'removed_by')],
                        'classes': ['collapse']}),
    ]

    list_display = ['title', 'publish_at', 'is_enabled', 'is_featured',
                    'is_indexable']

    list_filter = ['publish_at', 'is_enabled', 'is_featured', 'is_indexable']

    prepopulated_fields = {'slug': ['title']}

    readonly_fields = ['created_at', 'created_by', 'updated_at', 'updated_by']

    search_fields = ['title']

    inlines = [ProductCategoryInline]

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for instance in instances:
            if not hasattr(instance, 'created_by_id') \
                    or not instance.created_by_id:
                instance.created_by = request.user
            instance.updated_by = request.user
            instance.save()
        formset.save_m2m()


admin.site.register(Product, ProductAdmin)


class BrandAdmin(AuthorshipMixin, admin.ModelAdmin):

    fieldsets = [
        (None, {'fields': ['title', 'headline', 'featured_content', 'content',
                           'meta_description', 'tags']}),
        ('Publishing', {'fields': ['slug', ('publish_at', 'is_enabled'),
                                   'expire_at', 'publish_by', 'template',
                                   'is_featured', 'is_indexable',
                                   ('created_at', 'created_by'),
                                   ('updated_at', 'updated_by'),
                                   ('removed_at', 'removed_by')],
                        'classes': ['collapse']}),
    ]

    list_display = ['title', 'publish_at', 'is_enabled', 'is_featured',
                    'is_indexable']

    list_filter = ['publish_at', 'is_enabled', 'is_featured', 'is_indexable']

    prepopulated_fields = {'slug': ['title']}

    readonly_fields = ['created_at', 'created_by', 'updated_at', 'updated_by']

    search_fields = ['title']


admin.site.register(Brand, BrandAdmin)


class CategoryAdmin(AuthorshipMixin, admin.ModelAdmin):

    fieldsets = [
        (None, {'fields': ['title', 'headline', 'featured_content', 'content',
                           'meta_description', 'tags']}),
        ('Publishing', {'fields': ['slug', ('publish_at', 'is_enabled'),
                                   'expire_at', 'publish_by', 'template',
                                   'is_featured', 'is_indexable',
                                   ('created_at', 'created_by'),
                                   ('updated_at', 'updated_by'),
                                   ('removed_at', 'removed_by')],
                        'classes': ['collapse']}),
    ]

    list_display = ['title', 'publish_at', 'is_enabled', 'is_featured',
                    'is_indexable']

    list_filter = ['publish_at', 'is_enabled', 'is_featured', 'is_indexable']

    prepopulated_fields = {'slug': ['title']}

    readonly_fields = ['created_at', 'created_by', 'updated_at', 'updated_by']

    search_fields = ['title']


admin.site.register(Category, CategoryAdmin)


class SKUAttributeInline(admin.TabularInline):

    model = SKUAttribute
    verbose_name = 'SKU Attribute'
    verbose_name_plural = 'SKU Attributes'

    fieldsets = [
        (None, {'fields': ['attribute', 'value']}),
    ]


class SKUAdmin(AuthorshipMixin, admin.ModelAdmin):

    fieldsets = [
        (None, {'fields': ['product', 'sku']}),
        ('Publishing', {'fields': ['slug', ('publish_at', 'is_enabled'),
                                   'expire_at', 'publish_by', 'template',
                                   'is_featured', 'is_indexable',
                                   ('created_at', 'created_by'),
                                   ('updated_at', 'updated_by'),
                                   ('removed_at', 'removed_by')],
                        'classes': ['collapse']})
    ]

    list_display = ['sku', 'title', 'created_at']

    list_filter = ['created_at']

    readonly_fields = ['created_at', 'created_by', 'updated_at', 'updated_by']

    search_fields = ['title']

    inlines = [SKUAttributeInline]


admin.site.register(SKU, SKUAdmin)


class AttributeAdmin(admin.ModelAdmin):

    fieldsets = [
        (None, {'fields': ['name']}),
    ]

    list_display = ['name']
    search_fields = ['name']


admin.site.register(Attribute, AttributeAdmin)
