# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.conf.urls import include, patterns, url
from thecut.catalogue.views import attribute, brand, category, product, sku


urls = patterns(
    'thecut.catalogue.views',

    url(r'^categories/$',
        category.ListView.as_view(), name='category_list'),
    url(r'^categories/(?P<page>\d+)$',
        category.ListView.as_view(), name='paginated_category_list'),
    url(r'^category/(?P<slug>[\w-]+)$',
        category.DetailView.as_view(), name='category_detail'),

    url(r'^category/(?P<slug>[\w-]+)/products$',
        product.CategoryListView.as_view(), name='product_category_list'),
    url(r'^category/(?P<slug>[\w-]+)/products/(?P<page>\d+)$',
        product.CategoryListView.as_view(),
        name='paginated_product_category_list'),

    url(r'^brand/(?P<slug>[\w-]+)/products$',
        product.BrandListView.as_view(), name='product_brand_list'),
    url(r'^brand/(?P<slug>[\w-]+)/products/(?P<page>\d+)$',
        product.BrandListView.as_view(), name='paginated_product_brand_list'),

    url(r'^brands/$',
        brand.ListView.as_view(), name='brand_list'),
    url(r'^brands/(?P<page>\d+)$',
        brand.ListView.as_view(), name='paginated_brand_list'),
    url(r'^brand/(?P<slug>[\w-]+)$',
        brand.DetailView.as_view(), name='brand_detail'),

    url(r'^products/$',
        product.ListView.as_view(), name='product_list'),
    url(r'^products/(?P<page>\d+)$',
        product.ListView.as_view(), name='paginated_product_list'),
    url(r'^product/(?P<slug>[\w-]+)$',
        product.DetailView.as_view(), name='product_detail'),

    url(r'^skus/$',
        sku.ListView.as_view(), name='sku_list'),
    url(r'^skus/(?P<page>\d+)$',
        sku.ListView.as_view(), name='paginated_sku_list'),
    url(r'^sku/(?P<pk>\d+)$',
        sku.DetailView.as_view(), name='sku_detail'),

    url(r'^attributes/$',
        attribute.ListView.as_view(), name='attribute_list'),
    url(r'^attributes/(?P<page>\d+)$',
        attribute.ListView.as_view(), name='paginated_attribute_list'),
    url(r'^attributes/(?P<pk>\d+)$',
        attribute.DetailView.as_view(), name='attribute_detail'),
)

urlpatterns = patterns(
    '', (r'^', include(urls, namespace='catalogue')),
)
