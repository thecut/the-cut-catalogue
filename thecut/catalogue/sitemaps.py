# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.contrib.sitemaps import Sitemap
from django.core.paginator import Paginator
from django.core.urlresolvers import reverse
from thecut.catalogue import settings
from thecut.catalogue.models import Brand, Category, Product


class DumbObjectSitemap(Sitemap):
    """ Must be subclassed with 'model' class attribute set."""
    model = None

    def items(self):
        return self.model.objects.current_site().indexable()

    @staticmethod
    def lastmod(obj):
        return obj.updated_at


class ProductSitemap(DumbObjectSitemap):
    model = Product


class BrandSitemap(DumbObjectSitemap):
    model = Brand


class CategorySitemap(DumbObjectSitemap):
    model = Category


class DumbObjectListSitemap(Sitemap):
    """ Must be subclassed with class attributes 'model', 'main_url',
    'paginated_url' set."""
    model = None
    main_url = None
    paginated_url = None

    def items(self):
        objects = self.model.objects.current_site().indexable()
        page_size = settings.PRODUCTS_PAGINATE_BY
        return Paginator(objects, page_size).page_range if page_size else [1]

    def location(self, page):
        if page == 1:
            return reverse(self.main_url)
        else:
            return reverse(self.paginated_url, kwargs={'page': page})


class ProductListSitemap(DumbObjectListSitemap):
    model = Product
    main_url = 'product_list'
    paginated_url = 'paginated_product_list'


class CategoryListSitemap(DumbObjectListSitemap):
    model = Category
    main_url = 'category_list'
    paginated_url = 'paginated_category_list'


class BrandListSitemap(DumbObjectListSitemap):
    model = Product
    main_url = 'brand_list'
    paginated_url = 'paginated_brand_list'


class ProductCategoryListSitemap(Sitemap):

    def items(self):
        items = []
        for category in Category.current_site().indexable():
            products = Product.objects.for_category(category).indexable()
            page_size = settings.PRODUCTS_PAGINATE_BY
            page_range = Paginator(products, page_size).page_range \
                if page_size else [1]
            items += [(category.slug, page) for page in page_range]
        return items

    def location(self, opts):
        slug, page = opts
        if page == 1:
            return reverse('catalogue:product_category_list',
                           kwargs={'slug': slug})
        else:
            return reverse('catalogue:paginated_product_category_list',
                           kwargs={'slug': slug, 'page': page})


class ProductBrandListSitemap(Sitemap):

    def items(self):
        items = []
        for brand in Brand.current_site().indexable():
            products = Product.objects.for_brand(brand).indexable()
            page_size = settings.PRODUCTS_PAGINATE_BY
            page_range = Paginator(products, page_size).page_range \
                if page_size else [1]
            items += [(brand.slug, page) for page in page_range]
        return items

    def location(self, opts):
        slug, page = opts
        if page == 1:
            return reverse('catalogue:product_brand_list',
                           kwargs={'slug': slug})
        else:
            return reverse('catalogue:paginated_product_brand_list',
                           kwargs={'slug': slug, 'page': page})


sitemaps = {
    'catalogue_product': ProductSitemap,
    'catalogue_category': CategorySitemap,
    'catalogue_brand': BrandSitemap,
    'catalogue_product_list': ProductListSitemap,
    'catalogue_category_list': CategoryListSitemap,
    'catalogue_brand_list': BrandListSitemap,
    'catalogue_product_brand_list': ProductBrandListSitemap,
    'catalogue_product_category_list': ProductCategoryListSitemap
}
