# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models
from thecut.authorship.settings import AUTH_USER_MODEL


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Attribute'
        db.create_table(u'catalogue_attribute', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
        ))
        db.send_create_signal(u'catalogue', ['Attribute'])

        # Adding model 'Brand'
        db.create_table(u'catalogue_brand', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('created_by', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'+', to=orm[AUTH_USER_MODEL])),
            ('updated_at', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('updated_by', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'+', to=orm[AUTH_USER_MODEL])),
            ('is_enabled', self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True)),
            ('is_featured', self.gf('django.db.models.fields.BooleanField')(default=False, db_index=True)),
            ('publish_at', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now, db_index=True)),
            ('expire_at', self.gf('django.db.models.fields.DateTimeField')(db_index=True, null=True, blank=True)),
            ('publish_by', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name=u'+', null=True, to=orm[AUTH_USER_MODEL])),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('headline', self.gf('django.db.models.fields.CharField')(default=u'', max_length=200, blank=True)),
            ('content', self.gf('django.db.models.fields.TextField')(default=u'', blank=True)),
            ('featured_content', self.gf('django.db.models.fields.TextField')(default=u'', blank=True)),
            ('is_indexable', self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True)),
            ('meta_description', self.gf('django.db.models.fields.CharField')(default=u'', max_length=200, blank=True)),
            ('tags', self.gf('tagging.fields.TagField')(default=u'')),
            ('template', self.gf('django.db.models.fields.CharField')(default=u'', max_length=100, blank=True)),
            ('site', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['sites.Site'])),
            ('slug', self.gf('django.db.models.fields.SlugField')(max_length=50)),
        ))
        db.send_create_signal(u'catalogue', ['Brand'])

        # Adding unique constraint on 'Brand', fields ['site', 'slug']
        db.create_unique(u'catalogue_brand', ['site_id', 'slug'])

        # Adding model 'Category'
        db.create_table(u'catalogue_category', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('created_by', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'+', to=orm[AUTH_USER_MODEL])),
            ('updated_at', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('updated_by', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'+', to=orm[AUTH_USER_MODEL])),
            ('is_enabled', self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True)),
            ('is_featured', self.gf('django.db.models.fields.BooleanField')(default=False, db_index=True)),
            ('publish_at', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now, db_index=True)),
            ('expire_at', self.gf('django.db.models.fields.DateTimeField')(db_index=True, null=True, blank=True)),
            ('publish_by', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name=u'+', null=True, to=orm[AUTH_USER_MODEL])),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('headline', self.gf('django.db.models.fields.CharField')(default=u'', max_length=200, blank=True)),
            ('content', self.gf('django.db.models.fields.TextField')(default=u'', blank=True)),
            ('featured_content', self.gf('django.db.models.fields.TextField')(default=u'', blank=True)),
            ('is_indexable', self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True)),
            ('meta_description', self.gf('django.db.models.fields.CharField')(default=u'', max_length=200, blank=True)),
            ('tags', self.gf('tagging.fields.TagField')(default=u'')),
            ('template', self.gf('django.db.models.fields.CharField')(default=u'', max_length=100, blank=True)),
            ('site', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['sites.Site'])),
            ('slug', self.gf('django.db.models.fields.SlugField')(max_length=50)),
        ))
        db.send_create_signal(u'catalogue', ['Category'])

        # Adding unique constraint on 'Category', fields ['site', 'slug']
        db.create_unique(u'catalogue_category', ['site_id', 'slug'])

        # Adding model 'Product'
        db.create_table(u'catalogue_product', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('created_by', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'+', to=orm[AUTH_USER_MODEL])),
            ('updated_at', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('updated_by', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'+', to=orm[AUTH_USER_MODEL])),
            ('is_enabled', self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True)),
            ('is_featured', self.gf('django.db.models.fields.BooleanField')(default=False, db_index=True)),
            ('publish_at', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now, db_index=True)),
            ('expire_at', self.gf('django.db.models.fields.DateTimeField')(db_index=True, null=True, blank=True)),
            ('publish_by', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name=u'+', null=True, to=orm[AUTH_USER_MODEL])),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('headline', self.gf('django.db.models.fields.CharField')(default=u'', max_length=200, blank=True)),
            ('content', self.gf('django.db.models.fields.TextField')(default=u'', blank=True)),
            ('featured_content', self.gf('django.db.models.fields.TextField')(default=u'', blank=True)),
            ('is_indexable', self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True)),
            ('meta_description', self.gf('django.db.models.fields.CharField')(default=u'', max_length=200, blank=True)),
            ('tags', self.gf('tagging.fields.TagField')(default=u'')),
            ('template', self.gf('django.db.models.fields.CharField')(default=u'', max_length=100, blank=True)),
            ('site', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['sites.Site'])),
            ('slug', self.gf('django.db.models.fields.SlugField')(max_length=50)),
            ('brand', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name=u'products', null=True, to=orm['catalogue.Brand'])),
            ('primary_category', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name=u'+primary_products', null=True, to=orm['catalogue.Category'])),
        ))
        db.send_create_signal(u'catalogue', ['Product'])

        # Adding unique constraint on 'Product', fields ['site', 'slug']
        db.create_unique(u'catalogue_product', ['site_id', 'slug'])

        # Adding model 'ProductCategory'
        db.create_table(u'catalogue_productcategory', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('order', self.gf('django.db.models.fields.PositiveIntegerField')(default=0)),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('created_by', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'+', to=orm[AUTH_USER_MODEL])),
            ('updated_at', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('updated_by', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'+', to=orm[AUTH_USER_MODEL])),
            ('is_enabled', self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True)),
            ('is_featured', self.gf('django.db.models.fields.BooleanField')(default=False, db_index=True)),
            ('publish_at', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now, db_index=True)),
            ('expire_at', self.gf('django.db.models.fields.DateTimeField')(db_index=True, null=True, blank=True)),
            ('publish_by', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name=u'+', null=True, to=orm[AUTH_USER_MODEL])),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('headline', self.gf('django.db.models.fields.CharField')(default=u'', max_length=200, blank=True)),
            ('content', self.gf('django.db.models.fields.TextField')(default=u'', blank=True)),
            ('featured_content', self.gf('django.db.models.fields.TextField')(default=u'', blank=True)),
            ('is_indexable', self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True)),
            ('meta_description', self.gf('django.db.models.fields.CharField')(default=u'', max_length=200, blank=True)),
            ('tags', self.gf('tagging.fields.TagField')(default=u'')),
            ('template', self.gf('django.db.models.fields.CharField')(default=u'', max_length=100, blank=True)),
            ('site', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['sites.Site'])),
            ('slug', self.gf('django.db.models.fields.SlugField')(max_length=50)),
            ('product', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'+categoryproducts', to=orm['catalogue.Product'])),
            ('category', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'+categoryproducts', to=orm['catalogue.Category'])),
        ))
        db.send_create_signal(u'catalogue', ['ProductCategory'])

        # Adding unique constraint on 'ProductCategory', fields ['product', 'category']
        db.create_unique(u'catalogue_productcategory', ['product_id', 'category_id'])

        # Adding model 'SKU'
        db.create_table(u'catalogue_sku', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('created_by', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'+', to=orm[AUTH_USER_MODEL])),
            ('updated_at', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('updated_by', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'+', to=orm[AUTH_USER_MODEL])),
            ('is_enabled', self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True)),
            ('is_featured', self.gf('django.db.models.fields.BooleanField')(default=False, db_index=True)),
            ('publish_at', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now, db_index=True)),
            ('expire_at', self.gf('django.db.models.fields.DateTimeField')(db_index=True, null=True, blank=True)),
            ('publish_by', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name=u'+', null=True, to=orm[AUTH_USER_MODEL])),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('headline', self.gf('django.db.models.fields.CharField')(default=u'', max_length=200, blank=True)),
            ('content', self.gf('django.db.models.fields.TextField')(default=u'', blank=True)),
            ('featured_content', self.gf('django.db.models.fields.TextField')(default=u'', blank=True)),
            ('is_indexable', self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True)),
            ('meta_description', self.gf('django.db.models.fields.CharField')(default=u'', max_length=200, blank=True)),
            ('tags', self.gf('tagging.fields.TagField')(default=u'')),
            ('template', self.gf('django.db.models.fields.CharField')(default=u'', max_length=100, blank=True)),
            ('sku', self.gf('django.db.models.fields.CharField')(max_length=200, db_index=True)),
            ('product', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'skus', to=orm['catalogue.Product'])),
        ))
        db.send_create_signal(u'catalogue', ['SKU'])

        # Adding model 'SKUAttribute'
        db.create_table(u'catalogue_skuattribute', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('sku', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'attributes', to=orm['catalogue.SKU'])),
            ('attribute', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'+skus', to=orm['catalogue.Attribute'])),
            ('value', self.gf('django.db.models.fields.CharField')(max_length=200)),
        ))
        db.send_create_signal(u'catalogue', ['SKUAttribute'])

        # Adding unique constraint on 'SKUAttribute', fields ['sku', 'attribute']
        db.create_unique(u'catalogue_skuattribute', ['sku_id', 'attribute_id'])


    def backwards(self, orm):
        # Removing unique constraint on 'SKUAttribute', fields ['sku', 'attribute']
        db.delete_unique(u'catalogue_skuattribute', ['sku_id', 'attribute_id'])

        # Removing unique constraint on 'ProductCategory', fields ['product', 'category']
        db.delete_unique(u'catalogue_productcategory', ['product_id', 'category_id'])

        # Removing unique constraint on 'Product', fields ['site', 'slug']
        db.delete_unique(u'catalogue_product', ['site_id', 'slug'])

        # Removing unique constraint on 'Category', fields ['site', 'slug']
        db.delete_unique(u'catalogue_category', ['site_id', 'slug'])

        # Removing unique constraint on 'Brand', fields ['site', 'slug']
        db.delete_unique(u'catalogue_brand', ['site_id', 'slug'])

        # Deleting model 'Attribute'
        db.delete_table(u'catalogue_attribute')

        # Deleting model 'Brand'
        db.delete_table(u'catalogue_brand')

        # Deleting model 'Category'
        db.delete_table(u'catalogue_category')

        # Deleting model 'Product'
        db.delete_table(u'catalogue_product')

        # Deleting model 'ProductCategory'
        db.delete_table(u'catalogue_productcategory')

        # Deleting model 'SKU'
        db.delete_table(u'catalogue_sku')

        # Deleting model 'SKUAttribute'
        db.delete_table(u'catalogue_skuattribute')


    models = {
        AUTH_USER_MODEL: {
            'Meta': {'object_name': AUTH_USER_MODEL.split('.')[-1]},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'catalogue.attribute': {
            'Meta': {'ordering': "[u'name']", 'object_name': 'Attribute'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'catalogue.brand': {
            'Meta': {'ordering': "(u'title',)", 'unique_together': "((u'site', u'slug'),)", 'object_name': 'Brand'},
            'content': ('django.db.models.fields.TextField', [], {'default': "u''", 'blank': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'created_by': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'+'", 'to': u"orm[AUTH_USER_MODEL]"}),
            'expire_at': ('django.db.models.fields.DateTimeField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'}),
            'featured_content': ('django.db.models.fields.TextField', [], {'default': "u''", 'blank': 'True'}),
            'headline': ('django.db.models.fields.CharField', [], {'default': "u''", 'max_length': '200', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_enabled': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_featured': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_index': 'True'}),
            'is_indexable': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'meta_description': ('django.db.models.fields.CharField', [], {'default': "u''", 'max_length': '200', 'blank': 'True'}),
            'publish_at': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'db_index': 'True'}),
            'publish_by': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'+'", 'null': 'True', 'to': u"orm[AUTH_USER_MODEL]"}),
            'site': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['sites.Site']"}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'tags': ('tagging.fields.TagField', [], {'default': "u''"}),
            'template': ('django.db.models.fields.CharField', [], {'default': "u''", 'max_length': '100', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'updated_by': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'+'", 'to': u"orm[AUTH_USER_MODEL]"})
        },
        u'catalogue.category': {
            'Meta': {'ordering': "(u'title',)", 'unique_together': "((u'site', u'slug'),)", 'object_name': 'Category'},
            'content': ('django.db.models.fields.TextField', [], {'default': "u''", 'blank': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'created_by': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'+'", 'to': u"orm[AUTH_USER_MODEL]"}),
            'expire_at': ('django.db.models.fields.DateTimeField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'}),
            'featured_content': ('django.db.models.fields.TextField', [], {'default': "u''", 'blank': 'True'}),
            'headline': ('django.db.models.fields.CharField', [], {'default': "u''", 'max_length': '200', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_enabled': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_featured': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_index': 'True'}),
            'is_indexable': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'meta_description': ('django.db.models.fields.CharField', [], {'default': "u''", 'max_length': '200', 'blank': 'True'}),
            'publish_at': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'db_index': 'True'}),
            'publish_by': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'+'", 'null': 'True', 'to': u"orm[AUTH_USER_MODEL]"}),
            'site': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['sites.Site']"}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'tags': ('tagging.fields.TagField', [], {'default': "u''"}),
            'template': ('django.db.models.fields.CharField', [], {'default': "u''", 'max_length': '100', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'updated_by': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'+'", 'to': u"orm[AUTH_USER_MODEL]"})
        },
        u'catalogue.product': {
            'Meta': {'ordering': "(u'title',)", 'unique_together': "((u'site', u'slug'),)", 'object_name': 'Product'},
            'brand': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'products'", 'null': 'True', 'to': u"orm['catalogue.Brand']"}),
            'categories': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "u'products'", 'to': u"orm['catalogue.Category']", 'through': u"orm['catalogue.ProductCategory']", 'blank': 'True', 'symmetrical': 'False', 'null': 'True'}),
            'content': ('django.db.models.fields.TextField', [], {'default': "u''", 'blank': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'created_by': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'+'", 'to': u"orm[AUTH_USER_MODEL]"}),
            'expire_at': ('django.db.models.fields.DateTimeField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'}),
            'featured_content': ('django.db.models.fields.TextField', [], {'default': "u''", 'blank': 'True'}),
            'headline': ('django.db.models.fields.CharField', [], {'default': "u''", 'max_length': '200', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_enabled': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_featured': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_index': 'True'}),
            'is_indexable': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'meta_description': ('django.db.models.fields.CharField', [], {'default': "u''", 'max_length': '200', 'blank': 'True'}),
            'primary_category': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'+primary_products'", 'null': 'True', 'to': u"orm['catalogue.Category']"}),
            'publish_at': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'db_index': 'True'}),
            'publish_by': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'+'", 'null': 'True', 'to': u"orm[AUTH_USER_MODEL]"}),
            'site': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['sites.Site']"}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'tags': ('tagging.fields.TagField', [], {'default': "u''"}),
            'template': ('django.db.models.fields.CharField', [], {'default': "u''", 'max_length': '100', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'updated_by': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'+'", 'to': u"orm[AUTH_USER_MODEL]"})
        },
        u'catalogue.productcategory': {
            'Meta': {'ordering': "(u'order',)", 'unique_together': "[(u'product', u'category')]", 'object_name': 'ProductCategory'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'+categoryproducts'", 'to': u"orm['catalogue.Category']"}),
            'content': ('django.db.models.fields.TextField', [], {'default': "u''", 'blank': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'created_by': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'+'", 'to': u"orm[AUTH_USER_MODEL]"}),
            'expire_at': ('django.db.models.fields.DateTimeField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'}),
            'featured_content': ('django.db.models.fields.TextField', [], {'default': "u''", 'blank': 'True'}),
            'headline': ('django.db.models.fields.CharField', [], {'default': "u''", 'max_length': '200', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_enabled': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_featured': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_index': 'True'}),
            'is_indexable': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'meta_description': ('django.db.models.fields.CharField', [], {'default': "u''", 'max_length': '200', 'blank': 'True'}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'+categoryproducts'", 'to': u"orm['catalogue.Product']"}),
            'publish_at': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'db_index': 'True'}),
            'publish_by': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'+'", 'null': 'True', 'to': u"orm[AUTH_USER_MODEL]"}),
            'site': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['sites.Site']"}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'tags': ('tagging.fields.TagField', [], {'default': "u''"}),
            'template': ('django.db.models.fields.CharField', [], {'default': "u''", 'max_length': '100', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'updated_by': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'+'", 'to': u"orm[AUTH_USER_MODEL]"})
        },
        u'catalogue.sku': {
            'Meta': {'ordering': "(u'title',)", 'object_name': 'SKU'},
            'content': ('django.db.models.fields.TextField', [], {'default': "u''", 'blank': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'created_by': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'+'", 'to': u"orm[AUTH_USER_MODEL]"}),
            'expire_at': ('django.db.models.fields.DateTimeField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'}),
            'featured_content': ('django.db.models.fields.TextField', [], {'default': "u''", 'blank': 'True'}),
            'headline': ('django.db.models.fields.CharField', [], {'default': "u''", 'max_length': '200', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_enabled': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_featured': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_index': 'True'}),
            'is_indexable': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'meta_description': ('django.db.models.fields.CharField', [], {'default': "u''", 'max_length': '200', 'blank': 'True'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'skus'", 'to': u"orm['catalogue.Product']"}),
            'publish_at': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'db_index': 'True'}),
            'publish_by': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'+'", 'null': 'True', 'to': u"orm[AUTH_USER_MODEL]"}),
            'sku': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '200', 'db_index': 'True'}),
            'tags': ('tagging.fields.TagField', [], {'default': "u''"}),
            'template': ('django.db.models.fields.CharField', [], {'default': "u''", 'max_length': '100', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'updated_by': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'+'", 'to': u"orm[AUTH_USER_MODEL]"})
        },
        u'catalogue.skuattribute': {
            'Meta': {'ordering': "[u'attribute']", 'unique_together': "[(u'sku', u'attribute')]", 'object_name': 'SKUAttribute'},
            'attribute': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'+skus'", 'to': u"orm['catalogue.Attribute']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'sku': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'attributes'", 'to': u"orm['catalogue.SKU']"}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'sites.site': {
            'Meta': {'ordering': "(u'domain',)", 'object_name': 'Site', 'db_table': "u'django_site'"},
            'domain': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        }
    }

    complete_apps = ['catalogue']
