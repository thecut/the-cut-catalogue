# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.conf import settings


AUTH_USER_MODEL = getattr(settings, 'AUTH_USER_MODEL', 'auth.User')

PRODUCTS_PAGINATE_BY = getattr(settings, 'CATALOGUE_PRODUCTS_PAGINATE_BY', 10)
