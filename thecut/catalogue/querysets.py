# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.db import models


class AttributeQuerySet(models.query.QuerySet):
    def used_in_product(self, product):
        """
        Finds the attributes that are used in SKUs of given product.
        :returns: Filtered :py:class:'QuerySet'
        """
        return self.filter(skuattributes__sku__product=product)\
            .distinct().order_by('name')
