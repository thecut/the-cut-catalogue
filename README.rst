The Cut Django App: Catalogue


================
thecut.catalogue
================

To install this application (whilst in the project's activated virtualenv)::
    pip install git+ssh://git@git.thecut.net.au/thecut-catalogue


Add the ``thecut.catalogue`` to the project's ``INSTALLED_APPS`` setting::

    INSTALLED_APPS = (
        ...
        'thecut.catalogue',
    )
